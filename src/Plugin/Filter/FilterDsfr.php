<?php

declare(strict_types=1);

namespace Drupal\dsfr4drupal_editor\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Manage DSFR editor behaviors.
 *
 * @Filter(
 *   id = "filter_dsfr4drupal",
 *   title = @Translation("DSFR behaviors."),
 *   description = @Translation("Manage DSFR editor behaviors."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterDsfr extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $text = $this->processBlockquote($text);
    $text = $this->processMediaAlign($text);
    $text = $this->processTable($text);

    return new FilterProcessResult($text);
  }

  /**
   * Add DSFR wrapper to "table" html tag.
   *
   * @param string $text
   *   The text to process.
   *
   * @return string
   *   The processed text.
   */
  private function processTable(string $text): string {
    return str_replace(
      [
        '<table',
        '</table>',
      ],
      [
        '<div class="fr-table"><div class="fr-table__wrapper"><div class="fr-table__container"><div class="fr-table__content"><table',
        '</table></div></div></div></div>',
      ],
      $text
    );
  }

  /**
   * Add DSFR wrapper to "blockquote" html tag.
   *
   * @param string $text
   *   The text to process.
   *
   * @return string
   *   The processed text.
   */
  private function processBlockquote(string $text): string {
    return str_replace(
      [
        '<blockquote',
        '</blockquote>',
      ],
      [
        '<figure class="fr-quote"><blockquote',
        '</blockquote></figure>',
      ],
      $text
    );
  }

  /**
   * Add DSFR "text-align-(left|center|right)" class to media entity.
   *
   * @param string $text
   *   The text to process.
   *
   * @return string
   *   The processed text.
   */
  private function processMediaAlign(string $text): string {
    return preg_replace(
      '/<drupal-media([^>]*)class="([^"]*)align-(left|center|right)([^"]*)"([^>]*)>/s',
      '<drupal-media$1class="$2text-align-$3$4"$5>',
      $text
    );
  }

}
