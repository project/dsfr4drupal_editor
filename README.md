# DSFR for Drupal - Editor

## Introduction

This module allows behaviors to be managed by the "Design Système de l'État" ([DSFR](https://www.systeme-de-design.gouv.fr/)) on several components that can be contributed via CKEditor.

It provides a filter to activate on your rich text formats in order to be [DSFR](https://www.systeme-de-design.gouv.fr/) compliant.

Behaviors currently managed by the CKEditor filter:
* Blockquote component wrapper
* Embedded media alignment
* Table component wrapper

## Useful links

* [Quote component documentation](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/citation)
* [Table component documentation](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/tableau)
